import os
import youtube_dl

# Insert channel links form the channels you want archived here
# Names will be used as the name of the directory
channels = [
    {
        "name":"Techlore",
        "link":"https://www.youtube.com/channel/UCs6KfncB4OV6Vug4o_bzijg"
    },
    {
        "name":"The Hated One",
        "link":"https://www.youtube.com/channel/UCjr2bPAyPV7t35MvcgT3W8Q"
    }
]

# Create downloaded.txt file if it doesn't eixst
if not os.path.exists('downloaded.txt'):
    open('downloaded.txt', 'a').close()

for channel in channels:
    # Get current path
    script_path = os.path.dirname(os.path.realpath('__file__'))
    # Append folder name to current path
    save_to_path = os.path.join(script_path, channel['name'])

    # Make dir for channel if it doesn't exists
    if not os.path.exists(channel['name']):
        os.mkdir(channel['name'])
    
    # More options https://github.com/ytdl-org/youtube-dl/blob/3e4cedf9e8cd3157df2457df7274d0c842421945/youtube_dl/YoutubeDL.py#L137-L312
    options= {
        'format': 'best',  # choice of quality
        #'extractaudio': True,        # only keep the audio
        #'audioformat': "mp3",        # convert to mp3
        'outtmpl': f'{save_to_path}/[%(upload_date)s]%(title)s.%(ext)s',    # format for video title 
        #'listformats': True,         # print a list of the formats to stdout and
        'download_archive' : 'downloaded.txt',
        'nooverwrites': True,
        'ignoreerrors': True
    }

    ydl = youtube_dl.YoutubeDL(options)
    result = ydl.extract_info(
            channel['link'],
            download=True
    )
