# Youtube Channel Archiver

# Description
Simple python script to incrementally archive entire youtube channels using youtube-dl.

# Behaviour
The first time you run this script it will download all videos then only the ones missing.

You can stop the process and start it over to resume where it left with no problems, or at least minimal.

Video ids of downloaded videos will be saved in "downloaded.txt".  

You can run the script without having the *downloaded.txt* on videos **WITH THE SAME FORMAT** as the script produces and it will reconstruct the *downloaded.txt* 
without downloading the videos again. Note that it will take a bit longer than usual checking.

# Prerequisites
*  python 3
*  pip3
*  youtube-dl

On **Ubuntu**:

`sudo apt install python3`

`sudo apt install python3-pip`

`pip3 install youtube-dl`
